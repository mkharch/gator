//
//
//


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <poll.h>

#include <assert.h>
#include <malloc.h>
#include <string.h>

#include <unistd.h>

#include <libxl.h>
#include <libxl_utils.h>

#include "gator.h"
#include "listen.h"
#include "getput.h"

#define SLACK_FOR_LIBXL		256

#define MAX_CONSOLES		1024
#define MAX_CLOSE_IN_CHILD	8

struct conn_ctx_t *connections = 0;
int num_conns = 0;

libxl_ctx *new_context(void);

static void do_libxl_events(libxl_ctx *libxl);

static int connection_want_read(struct conn_ctx_t *conn);
static int connection_want_write(struct conn_ctx_t *conn);
static int connection_read(libxl_ctx *libxl, struct conn_ctx_t *conn);
static int connection_write(libxl_ctx *libxl, struct conn_ctx_t *conn);

static int decode_request(uint8_t *packed, int size, struct request_data *req_data);
static int fork_console(libxl_ctx *ctx,
		int fd, uint32_t domid, libxl_evgen_domain_death *death);

static int destroy_attached_domain(libxl_ctx *ctx, pid_t child);
static int detach_domain_console(uint32_t domid);

static struct {
	pid_t pid;
	uint32_t domid;
	libxl_evgen_domain_death *death;
} active_consoles[MAX_CONSOLES];
static int num_consoles = 0;

int close_in_child[MAX_CLOSE_IN_CHILD];
int num_close_in_child = 0;

void start_listening(char *bind_to, int listen_port, int verbose)
{
	int timeout;
	struct timeval now;

	int listen_sock = socket(AF_INET, SOCK_STREAM, 0);

	assert(num_close_in_child < MAX_CLOSE_IN_CHILD);
	close_in_child[num_close_in_child++] = listen_sock;

	struct sockaddr_in addr = {
		.sin_family = AF_INET,
		.sin_port = ntohs(listen_port),
		.sin_addr = INADDR_ANY,
	};
	int rs = inet_aton(bind_to, &addr.sin_addr);
	assert(rs != 0);

	int on = 1;
	rs = setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR,
						(char *)&on, sizeof(on));
	assert(rs == 0);

	rs = bind(listen_sock,
			(const struct sockaddr *)&addr, sizeof(addr));
	assert(rs == 0);

	rs = listen(listen_sock, 1024);
	assert(rs == 0);

	libxl_ctx *libxl = new_context();

	for (;;)
	{
		struct pollfd pollfds[num_conns +1 +SLACK_FOR_LIBXL];

		struct conn_ctx_t *conn = connections;
		struct pollfd *pfd = pollfds;
		while (conn != 0)
		{
			int events = 0;
			if (connection_want_read(conn))
				events |= POLLIN;
			if (connection_want_write(conn))
				events |= POLLOUT;

			pfd->fd = conn->fd;
			pfd->events = events;
			pfd++;

			conn = conn->next;
		}
		assert(pfd == pollfds +num_conns);

		struct pollfd *lpfd = pollfds +num_conns;
		lpfd->fd = listen_sock;
		lpfd->events = POLLIN;

try_again:
		timeout = 5000;
		gettimeofday(&now, 0);

		int nfds_io = SLACK_FOR_LIBXL;
		rs = libxl_osevent_beforepoll(libxl,
				&nfds_io, pollfds +num_conns +1, &timeout, now);
		assert(rs == 0);

		if (verbose > 2)
			printf("poll: fds %d timeout %d\n", 1 +num_conns +nfds_io, timeout);

		rs = poll(pollfds, num_conns +1 +nfds_io, timeout);
		if (rs == -1)
		{
			// related to ignored progress report?
			if (verbose > 2)
				printf("poll() returns %d, retrying...\n", rs);
			goto try_again;
		}
		assert(rs >= 0);

		//gettimeofday(&now, 0);
		libxl_osevent_afterpoll(libxl, nfds_io, pollfds +num_conns +1, now);

		// libxl events first
		do_libxl_events(libxl);

		// connection events
		conn = connections;
		pfd = pollfds;
		while (pfd < pollfds +num_conns)
		{
			if (pfd->revents & POLLERR)
				conn->err = -1;
			if (!conn->err && (pfd->revents & POLLIN))
				conn->err = connection_read(libxl, conn);
			if (!conn->err && (pfd->revents & POLLOUT))
				conn->err = connection_write(libxl, conn);
			pfd++;
			
			conn = conn->next;
		}

		lpfd = pollfds +num_conns;
		int accept_pending = (lpfd->revents & POLLIN);

		// cleanup failing/closing connections
		conn = connections;
		while (conn != 0)
		{
			if (conn->state == GTR_STATE_DONE || conn->err)
			{
				close(conn->fd);
				if (verbose > 1)
					printf("Connection closed: fd %d\n", conn->fd);

				// conn_ctx_t gets disposed of (the only such place)
				free(conn->data);

				*conn->ref = conn->next;
				if (conn->next)
					conn->next->ref = conn->ref;

				struct conn_ctx_t *free_me = conn;
				conn = conn->next;

				free(free_me);
				num_conns--;
			}
			else
				conn = conn->next;
		}

		if (accept_pending)
		{
			int new_fd = accept(listen_sock, 0, 0);
			struct conn_ctx_t *new_conn = malloc(sizeof(*new_conn));

			// conn_ctx_t gets initialized (the only such place)
			memset(new_conn, 0, sizeof(*new_conn));
			new_conn->fd = new_fd;
			new_conn->state = GTR_STATE_REQ;

			new_conn->next = connections;
			if (new_conn->next)
				new_conn->next->ref = &new_conn->next;
			connections = new_conn;
			new_conn->ref = &connections;
			num_conns++;

			if (verbose > 1)
				printf("New connection accepted: fd %d\n", new_fd);
		}

		// get rid of xenconsole zombies
		int status;
		pid_t exited;

more_children:
		exited = waitpid(-1, &status, WNOHANG);
		if (exited > 0)
		{
			printf("xenconsole %d exits\n", exited);
			destroy_attached_domain(libxl, exited);
			goto more_children;
		}
	}
}

static void do_libxl_events(libxl_ctx *libxl)
{
	for (;;)
	{
		libxl_event *event;
		int rs = libxl_event_check(libxl, &event, LIBXL_EVENTMASK_ALL, 0, 0);
		if (rs == ERROR_NOT_READY)
			break;
		assert (rs == 0);

		if (event->type == LIBXL_EVENT_TYPE_OPERATION_COMPLETE)
		{
			struct conn_ctx_t *conn = (struct conn_ctx_t *)event->for_user;

			if (conn->action_complete)
				conn->action_complete(libxl, conn);

			int op_result = event->u.operation_complete.rc;
			if (op_result != 0)
				connection_notify_error(conn, op_result);
			else
				connection_notify_ok(conn);
		}
		else if (event->type == LIBXL_EVENT_TYPE_DOMAIN_SHUTDOWN)
		{
			int ret = libxl_domain_destroy(libxl, event->domid, 0);
			assert(ret == 0);
			printf("Domain %d shut down\n", event->domid);
		}
		else 
		{
			assert(event->type == LIBXL_EVENT_TYPE_DOMAIN_DEATH);
			detach_domain_console(event->domid);
			printf("Domain %d destroyed\n", event->domid);
		}

		libxl_event_free(libxl, event);
	}
}

static int connection_want_read(struct conn_ctx_t *conn)
{
	return conn->state == GTR_STATE_REQ;
}

static int connection_want_write(struct conn_ctx_t *conn)
{
	return conn->state == GTR_STATE_RSP;
}

static int connection_read(libxl_ctx *libxl, struct conn_ctx_t *conn)
{
	assert(conn->state == GTR_STATE_REQ);
	if (conn->num_read < HDR_SIZE)
	{
		int nr = read(conn->fd, conn->header +conn->num_read,
								HDR_SIZE -conn->num_read);
		if (nr <= 0)
			return -1;
		conn->num_read += nr;
		if (conn->num_read == HDR_SIZE)
		{
			uint32_t magic = GET_UINT_32(conn->header);
			if (magic != GTR_MAGIC1)
			{
				printf("Bad magic number - closing connection\n");
				return -1;
			}
			conn->data_size = GET_UINT_32(conn->header +4);
			assert(conn->data_size > HDR_SIZE);
			assert(conn->data == 0);
			conn->data = malloc(conn->data_size);
			memcpy(conn->data, conn->header, HDR_SIZE);
		}
	}
	else
	{
		assert(conn->num_read <= conn->data_size);
		int nr = read(conn->fd, conn->data +conn->num_read,
								conn->data_size -conn->num_read);
		if (nr <= 0)
			return -1;
		conn->num_read += nr;
		if (conn->num_read == conn->data_size)
		{
			struct request_data req_data;
			int rs = decode_request(conn->data +HDR_SIZE,
							conn->num_read -HDR_SIZE, &req_data);
			if (rs != 0)
				return rs;

			free(conn->data);
			conn->data = 0;
			conn->state = GTR_STATE_PEND;

			rs = start_action(libxl, &req_data, conn);

			free(req_data.kernel);
			free(req_data.name);
			free(req_data.extra);
			free(req_data.bridge);
			free(req_data.prefix);
			free(req_data.disk);

			if (rs != 0)
				connection_notify_error(conn, rs);

			do_libxl_events(libxl);
		}
	}

	return 0;
}

static int connection_write(libxl_ctx *libxl, struct conn_ctx_t *conn)
{
	assert(conn->state == GTR_STATE_RSP);
	assert(conn->num_written < conn->data_size);

	int nw = write(conn->fd, conn->data +conn->num_written,
							 conn->data_size -conn->num_written);
	if (nw <= 0)
		return -1;
	conn->num_written += nw;
	if (conn->num_written == conn->data_size)
	{
		conn->state = GTR_STATE_DONE;
		
		if (conn->fork_console_for_domid != 0)
		{
			int ret = fork_console(libxl, conn->fd,
						conn->fork_console_for_domid, conn->death);
			if (ret < 0)
				printf("Cannot fork() console: %d\n", ret);
		}
	}

	return 0;
}

void connection_notify_error(struct conn_ctx_t *conn, int rs)
{
	assert(conn->state == GTR_STATE_PEND);
	conn->data_size = 4 +4 +2 +4;
	assert(conn->data == 0);
	conn->data = malloc(conn->data_size);
	PUT_UINT_32(conn->data, GTR_MAGIC2);
	PUT_UINT_32(conn->data +4, conn->data_size);
	PUT_UINT_16(conn->data +8, GTR_TAG_ERROR);
	PUT_UINT_32(conn->data +10, rs);
	
	conn->num_written = 0;
	conn->state = GTR_STATE_RSP;
}

void connection_notify_ok(struct conn_ctx_t *conn)
{
	assert(conn->state == GTR_STATE_PEND);
	conn->data_size = 4 +4 +2;
	assert(conn->data == 0);
	conn->data = malloc(conn->data_size);
	PUT_UINT_32(conn->data, GTR_MAGIC2);
	PUT_UINT_32(conn->data +4, conn->data_size);
	PUT_UINT_16(conn->data +8, GTR_TAG_OK);
	
	conn->num_written = 0;
	conn->state = GTR_STATE_RSP;
}

static int decode_request(uint8_t *packed, int size, struct request_data *req_data)
{
	memset(req_data, 0, sizeof(*req_data));
	req_data->memory = DEFAULT_MEMORY;
	req_data->bridge = strdup(DEFAULT_BRIDGE);

	uint8_t *ptr = packed;
	int left = size;
	while (left > 0)
	{
		if (left < 2)
			return -1;
		uint16_t tag = GET_UINT_16(ptr);
		ptr += 2;
		left -= 2;
		switch (tag) {
		case GTR_TAG_ACTION:
			if (left < 4)
				return -1;
			req_data->action = GET_UINT_32(ptr);
			ptr += 4;
			left -= 4;
			break;
		case GTR_TAG_KERNEL:
			if (left < 2)
				return -1;
			int len = GET_UINT_16(ptr);
			ptr += 2;
			left -= 2;
			if (len > left)
				return -1;
			free(req_data->kernel);
			req_data->kernel = strndup(ptr, len);
			ptr += len;
			left -= len;
			break;
		case GTR_TAG_NAME:
			if (left < 2)
				return -1;
			len = GET_UINT_16(ptr);
			ptr += 2;
			left -= 2;
			if (len > left)
				return -1;
			free(req_data->name);
			req_data->name = strndup(ptr, len);
			ptr += len;
			left -= len;
			break;
		case GTR_TAG_MEMORY:
			if (left < 4)
				return -1;
			req_data->memory = GET_UINT_32(ptr);
			ptr += 4;
			left -= 4;
			break;
		case GTR_TAG_EXTRA:
			if (left < 2)
				return -1;
			len = GET_UINT_16(ptr);
			ptr += 2;
			left -= 2;
			if (len > left)
				return -1;
			free(req_data->extra);
			req_data->extra = strndup(ptr, len);
			ptr += len;
			left -= len;
			break;
		case GTR_TAG_BRIDGE:
			if (left < 2)
				return -1;
			len = GET_UINT_16(ptr);
			ptr += 2;
			left -= 2;
			if (len > left)
				return -1;
			free(req_data->bridge);
			req_data->bridge = strndup(ptr, len);
			ptr += len;
			left -= len;
			break;
		case GTR_TAG_NOVIF:
			req_data->novif = 1;
			break;
		case GTR_TAG_DISK:
			if (left < 2)
				return -1;
			len = GET_UINT_16(ptr);
			ptr += 2;
			left -= 2;
			if (len > left)
				return -1;
			free(req_data->disk);
			req_data->disk = strndup(ptr, len);
			ptr += len;
			left -= len;
			break;
		case GTR_TAG_RDONLY:
			req_data->rdonly = 1;
			break;
		case GTR_TAG_CONSOLE:
			req_data->console = 1;
			break;
		case GTR_TAG_PREFIX:
			if (left < 2)
				return -1;
			len = GET_UINT_16(ptr);
			ptr += 2;
			left -= 2;
			if (len > left)
				return -1;
			free(req_data->prefix);
			req_data->prefix = strndup(ptr, len);
			ptr += len;
			left -= len;
			break;
		default:
			return -1;
		}
	}

	return 0;
}

static int fork_console(libxl_ctx *ctx,
		int fd, uint32_t domid, libxl_evgen_domain_death *death)
{
	pid_t child = fork();
	if (child < 0)
		return child;

	if (child == 0)
	{
		printf("xenconsole %d forked for domid %d\n", getpid(), domid);

		libxl_postfork_child_noexec(ctx);
		ctx = 0;

		// libxl_postfork_child_noexec does roughly the same
		int i;
		for (i = 0; i < num_close_in_child; i++)
			close(close_in_child[i]);

		dup2(fd, STDOUT_FILENO);
		dup2(fd, STDIN_FILENO);

		ctx = new_context();
		libxl_primary_console_exec(ctx, domid);

		// No return
	}
	else
	{
		// save pid <--> domid pair to destroy domain as needed
		//
		assert(num_consoles < MAX_CONSOLES);
		active_consoles[num_consoles].pid = child;
		active_consoles[num_consoles].domid = domid;
		active_consoles[num_consoles].death = death;
		num_consoles++;
	}

	return 0;
}

static int destroy_attached_domain(libxl_ctx *ctx, pid_t child)
{
	int i;
	for (i = 0; i < num_consoles; i++)
	{
		if (active_consoles[i].pid == child)
		{
			// infinite loop if event not disabled
			libxl_evdisable_domain_death(ctx, active_consoles[i].death);

			uint32_t domid = active_consoles[i].domid;
			int ret = libxl_domain_destroy(ctx, domid, 0);
			if (ret != 0)
				printf("libxl_domain_destroy: error ignored: %d\n", ret);

			if (num_consoles > 1)
				active_consoles[i] = active_consoles[num_consoles -1];
			num_consoles--;

			printf("Domain %d attached to console %d destroyed\n", domid, child);
			break;
		}
	}

	return 0;
}

static int detach_domain_console(uint32_t domid)
{
	int i;
	for (i = 0; i < num_consoles; i++)
	{
		if (active_consoles[i].domid == domid)
		{
			pid_t child = active_consoles[i].pid;

			if (num_consoles > 1)
				active_consoles[i] = active_consoles[num_consoles -1];
			num_consoles--;

			printf("Domain %d detached from console %d\n", domid, child);
			break;
		}
	}

	return 0;
}

//EOF
