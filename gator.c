//
//
//

#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <libgen.h>
#include <assert.h>

#include <xentoollog.h>

#include <libxl.h>
#include <libxl_utils.h>

#include "gator.h"

static 	int		opt_verbose = 0;
static 	int		opt_count = 1;
static 	char	*opt_name = 0;
static 	int		opt_memory = DEFAULT_MEMORY;
static 	char	*opt_kernel = 0;
static 	char	*opt_extra = 0;
static 	char	*opt_bridge = 0;
static	int		opt_has_vif = 1;
static	char	*opt_disk = 0;
static	int		opt_read_only = 0;
static	int		opt_console = 0;

static	int		opt_port = DEFAULT_PORT;
static	char	*opt_bind_to = DEFAULT_ADDR;

static	int		opt_cleanup = 0;
static	int		opt_listen = 0;

static int populate_domain_config(libxl_domain_config *dc);

libxl_ctx *new_context();

static void do_cleanup(libxl_ctx *ctx);
static void do_create(libxl_ctx *ctx);

static void handle_death(libxl_ctx *ctx, uint32_t domid);

static void usage(void);

void start_listening(char *bind_to, int listen_port, int verbose);

int main(int argc, char *argv[])
{
	while (1)
	{
		static struct option long_opts[] = {
			{"verbose",	no_argument,		0, 'v'},
			{"count", 	required_argument,	0, 'c'},
			{"name",	required_argument,	0, 'n'},
			{"memory",	required_argument,	0, 'm'},
			{"kernel",	required_argument,	0, 'k'},
			{"extra",	required_argument,	0, 'e'},
			{"bridge",	required_argument,	0, 'b'},
			{"port",	required_argument,	0, 'p'},
			{"bind-to", required_argument,	0, 'i'},
			{"no-vif",	no_argument,		&opt_has_vif, 0},
			{"disk",	required_argument,	0, 'd' },
			{"read-only", no_argument,		&opt_read_only, 1},
			{"console", no_argument,		&opt_console, 1},
			{"cleanup", no_argument,		&opt_cleanup, 1},
			{"listen",	no_argument,		&opt_listen, 1},
			{"help",	no_argument,		0, 'h'},
			{0, 0, 0, 0}
		};

		int opt_index;
		int c = getopt_long(argc, argv, "vc:n:m:k:e:b:lh", long_opts, &opt_index);
		if (c == -1)
			break;

		switch (c)
		{
		case 0:
			break;

		case 'v':
			opt_verbose++;
			break;

		case 'c':
			opt_count = atoi(optarg);
			if (opt_count <= 0)
			{
				opt_count = 1;
				printf("Warning: number of instances must be > 0; set to 1\n");
			}
			break;

		case 'n':
			opt_name = optarg;
			break;

		case 'm':
			opt_memory = atoi(optarg);
			if (opt_memory <= 0)
			{
				opt_memory = DEFAULT_MEMORY;
				printf("Warning: memory size must be > 0; set to %d\n", opt_memory);
			}
			break;

		case 'k':
			opt_kernel = optarg;
			break;

		case 'e':
			opt_extra = optarg;
			break;

		case 'b':
			opt_bridge = optarg;
			break;

		case 'p':
			opt_port = atoi(optarg);
			if (opt_port <= 0)
			{
				printf("Error: invalid port number: %s\n", optarg);
				exit(1);
			}
			break;

		case 'i':	// --bind-to
			opt_bind_to = optarg;
			break;

		case 'd':
			opt_disk = optarg;
			break;

		case 'h':
			usage();
			exit(0);
		}
	}

	if (opt_listen && opt_cleanup)
	{
		printf("Error: cannot use both --listen and --cleanup\n");
		exit(1);
	}

	if (opt_console && opt_cleanup)
	{
		printf("Error: cannot use both --console and --cleanup\n");
		exit(1);
	}

	if (opt_listen)
	{
		// warn about extra options?
		start_listening(opt_bind_to, opt_port, opt_verbose);
		return 0;
	}
	else if (opt_cleanup)
	{
		if (opt_name == 0)
		{
			printf("Error: domain name must be set for cleanup operation; use --name NAME\n");
			exit(1);
		}
	}
	else // create
	{
		if (opt_kernel == 0)
		{
			printf("Error: no kernel image provided; use --kernel IMAGE\n");
			exit(1);
		}

		if (opt_has_vif == 0 && opt_bridge != 0)
		{
			printf("Error: both --no-vif and --bridge are provided\n");
			exit(1);
		}

		if (opt_has_vif == 1 && opt_bridge == 0)
		{
			opt_bridge = DEFAULT_BRIDGE;
			if (opt_verbose > 1)
				printf("Warning: bridge name set to '%s'\n", DEFAULT_BRIDGE);
		}

		if (opt_name == 0)
		{
			assert(opt_kernel != 0);
			char *stem = strdup(opt_kernel);
			stem = basename(stem);
			char *dotp = strrchr(stem, '.');
			if (dotp != 0)
				*dotp = 0;
			opt_name = stem;
			if (opt_verbose > 0)
				printf("Warning: domain name '%s' derived from image file name;"
							" use --name to override\n", opt_name);
		}

		if (opt_console && opt_count > 1)
		{
			printf("Warning: --console option ignored when creating multiple domains\n");
			opt_console = 0;
		}
	}

	libxl_ctx *ctx = new_context();

	if (opt_cleanup)
		do_cleanup(ctx);
	else
		do_create(ctx);

	int rs = libxl_ctx_free(ctx);
	assert(rs == 0);
}

libxl_ctx *new_context()
{
	xentoollog_level log_level = XTL_ERROR;
	if (opt_verbose >= 1)
		log_level = XTL_WARN;
	if (opt_verbose >= 2)
		log_level = XTL_INFO;
	if (opt_verbose >= 3)
		log_level = XTL_DEBUG;
	xentoollog_logger_stdiostream *logger =
			xtl_createlogger_stdiostream(stdout, log_level, 0);
	assert(logger != 0);

	libxl_ctx *ctx;
	int rs = libxl_ctx_alloc(&ctx, 0, 0, (xentoollog_logger *)logger);
	assert(rs == 0);

	return ctx;
}

static void do_create(libxl_ctx *ctx)
{
	int rs;

	struct timespec time_started, time_completed;
	clock_gettime(CLOCK_MONOTONIC, &time_started);

	struct creation {
		libxl_domid domid;
		libxl_domain_config config;
	} creations[opt_count];

	// initiate creation
	int i;
	for (i = 0; i < opt_count; i++)
	{
		struct creation *creat = &creations[i];

		rs = populate_domain_config(&creat->config);
		assert(rs == 0);

		if (opt_count > 1)
		{
			char *numbered_name;
			asprintf(&numbered_name, "%s%d", creat->config.c_info.name, i +1);
			free(creat->config.c_info.name);
			creat->config.c_info.name = numbered_name;
		}

		libxl_asyncop_how ao_how = {
			.u.for_event = (libxl_ev_user) creat,
		};

		libxl_asyncprogress_how *ao_console_how = 0;
		libxl_asyncprogress_how ao_console_how_buf = { .callback = 0 };
		if (opt_console)
			ao_console_how = &ao_console_how_buf;
	
		uint32_t domid;
		rs = libxl_domain_create_new(ctx,
					&creat->config, &creat->domid, &ao_how, ao_console_how);
		assert(rs == 0);

		if (opt_verbose)
			printf("Creation initiated for '%s'\n", opt_name);
	}

	if (opt_verbose)
		printf("Waiting for completion...\n");

	int num_completed = 0;
	int num_created = 0;

	pid_t console_pid = 0;

	while (num_completed < opt_count )
	{
		libxl_event *ev;
		libxl_event_wait(ctx, &ev, LIBXL_EVENTMASK_ALL, 0, 0);

		if (ev->type == LIBXL_EVENT_TYPE_OPERATION_COMPLETE)
		{
			struct creation *creat = (struct creation *)ev->for_user;

			if (ev->u.operation_complete.rc != 0)
			{
				printf("Error: failed to create domain: %d\n",
						ev->u.operation_complete.rc);
			}
			else
			{
				if (opt_verbose)
					printf("Domain %d created\n", creat->domid);

				rs = libxl_domain_unpause(ctx, creat->domid);
				assert(rs == 0);

				num_created++;
			}

			libxl_domain_config_dispose(&creat->config);
			num_completed++;	//XXX lock?
		}
		else
		{
			assert(ev->type == LIBXL_EVENT_TYPE_DOMAIN_CREATE_CONSOLE_AVAILABLE);

			console_pid = fork();
			assert(console_pid >= 0);

			if (console_pid == 0)
			{
				if (opt_verbose)
					printf("xenconsole forked %d\n", getpid());

				libxl_postfork_child_noexec(ctx);
				ctx = 0;

				//sleep(1);
				ctx = new_context();
				libxl_primary_console_exec(ctx, ev->domid);	
				// Does not return
			}
		}

		libxl_event_free(ctx, ev);
	}

	if (opt_console)
	{
		assert(console_pid > 0);

		handle_death(ctx, creations[0].domid);

		if (opt_verbose)
			printf("Waiting for %d to terminate...\n", console_pid);

		int status;
		waitpid(console_pid, &status, 0);
		assert(WIFEXITED(status));

		if (opt_verbose)
			printf("xenconsole exits [%d]\n", WEXITSTATUS(status));
	}
	else
	{
		clock_gettime(CLOCK_MONOTONIC, &time_completed);
		double elapsed_sec = (time_completed.tv_sec - time_started.tv_sec) +
			(time_completed.tv_nsec - time_started.tv_nsec) *1e-9;
		if (num_created == 0)
			printf("No domains created\n");
		else
		{
			printf("%d domain(s) created in %.3f sec(s)\n", num_created, elapsed_sec);
			printf("avg creation time: %.3f sec(s)/dom\n", elapsed_sec /num_created);
			printf("creation rate: %.3f dom(s)/sec\n", num_created /elapsed_sec);
		}
	}
}

static void handle_death(libxl_ctx *ctx, uint32_t domid)
{
	libxl_evgen_domain_death *death_out;
	int ret = libxl_evenable_domain_death(ctx, domid, 0, &death_out);
	assert(ret == 0);

	if (opt_verbose)
		printf("Waiting for domain %d shutdown\n", domid);

	libxl_event *ev;
	libxl_event_wait(ctx, &ev, LIBXL_EVENTMASK_ALL, 0, 0);
	
	if (ev->type == LIBXL_EVENT_TYPE_DOMAIN_SHUTDOWN)
	{
		if (opt_verbose)
			printf("Domain %d shuts down\n", domid);
		int ret = libxl_domain_destroy(ctx, domid, 0);
		assert(ret == 0);
		printf("Domain %d destroyed\n", domid);
	}
	else
	{
		assert(ev->type == LIBXL_EVENT_TYPE_DOMAIN_DEATH);
		if (opt_verbose)
			printf("Domain %d destroyed?\n", domid);
	}

	libxl_event_free(ctx, ev);
}

static void do_cleanup(libxl_ctx *ctx)
{
	int rs;
	int num_doms;
	libxl_dominfo *dom_list = libxl_list_domain(ctx, &num_doms);
	assert(dom_list != 0);

	int num_destroyed = 0;

	int i;
	for (i = 0; i < num_doms; i++)
	{
		libxl_domid domid = dom_list[i].domid;
		char *name = libxl_domid_to_name(ctx, domid);
		int is_victim = 0;
		if (opt_count == 1 && strcmp(opt_name, name) == 0)
			is_victim = 1;
		int prefix_len = strlen(opt_name);
		if (opt_count > 1 && strncmp(opt_name, name, prefix_len) == 0)
		{
			int ord = atoi(name +prefix_len);
			is_victim = ord > 0 && ord <= opt_count;
		}

		// async?
		if (is_victim)
		{
			rs = libxl_domain_destroy(ctx, domid, 0);
			assert(rs == 0);
			if (opt_verbose)
				printf("Domain %s destroyed\n", name);
			num_destroyed++;
		}
	}

	printf("%d domain(s) destroyed\n", num_destroyed);

	libxl_dominfo_list_free(dom_list, num_doms);
}

static int populate_domain_config(libxl_domain_config *dc)
{
	assert(opt_name != 0);
	assert(opt_memory > 0);
	assert(opt_kernel != 0);
	assert(!opt_has_vif || opt_bridge != 0);

	libxl_domain_config_init(dc);

	libxl_domain_create_info *c_info = &dc->c_info;
	libxl_domain_build_info *b_info = &dc->b_info;

	c_info->type = LIBXL_DOMAIN_TYPE_PV;
	c_info->name = strdup(opt_name);

	libxl_domain_build_info_init_type(b_info, c_info->type);

	b_info->max_memkb = opt_memory *1024;
	b_info->target_memkb = opt_memory *1024;

	if (opt_extra != 0)
		b_info->u.pv.cmdline = strdup(opt_extra);
	b_info->u.pv.kernel = strdup(opt_kernel);

	if (opt_has_vif)
	{
		libxl_device_nic *nic = malloc(sizeof(libxl_device_nic));
		libxl_device_nic_init(nic);

		//nic->script = strdup("/etc/xen/scripts/vif-bridge");
		nic->bridge = strdup(opt_bridge);

		dc->num_nics = 1;
		dc->nics = nic;
	}
	
	if (opt_disk != 0)
	{
		libxl_device_disk *dsk = malloc(sizeof(libxl_device_disk));
		libxl_device_disk_init(dsk);

		dsk->readwrite = !opt_read_only;
		dsk->format = LIBXL_DISK_FORMAT_RAW;
		dsk->pdev_path = strdup(opt_disk);
		dsk->vdev = strdup("xvda");

		dc->num_disks = 1;
		dc->disks = dsk;
	}

	return 0;
}

static void usage(void)
{
	printf("Usage: gator [OPTIONS] - create/cleanup domains asynchronously\n"
			"                         or serve network connections\n"
			"General options:\n"
			"    --verbose   - be verbose; use many to increase verbosity\n"
			"    --help      - prints this help\n"
			"\n"
			"Listening:\n"
			"    gator --listen [--port PORT] [--bind-to IPADDR]\n"
			"\n"
			"Creating domains:\n"
			"	gator --kernel IMAGE [--name NAME] [--memory MB] [--extra CMDLINE]\n"
			"          [--bridge BRIDGE] [--no-vif] [--console]\n"
			"          [--count COUNT]\n"
			"\n"
			"    --kernel    - a kernel image file name\n"
			"    --name      - a domain name\n"
			"    --memory    - a domain memory size in MB; defaults to 128\n"
			"    --extra     - a command line to pass to the kernel\n"
			"    --bridge    - a name of brdige interface; defaults to br0\n"
			"    --no-vif    - do not create a virtual network interface\n"
			"    --disk      - a disk image to attach\n"
			"    --read-only - make the attached disk read-only\n"
			"    --console   - connect to console after creation\n"
			"    --count     - a number of copies to create; default to 1\n"
			"\n"
			"Destroying domains:\n"
			"    gator --cleanup --name NAME [--count]\n"
			"\n");
}

//EOF
