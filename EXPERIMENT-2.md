
creep version: commit f4c7c6

'xl info' output:

	host                   : dom0
	release                : 3.7.10-1.11-xen
	version                : #1 SMP Thu May 16 20:27:27 UTC 2013 (adf31bb)
	machine                : x86_64
	nr_cpus                : 8
	max_cpu_id             : 7
	nr_nodes               : 1
	cores_per_socket       : 4
	threads_per_core       : 2
	cpu_mhz                : 3400
	hw_caps                :
	bfebfbff:28100800:00000000:00007f40:77bae3ff:00000000:00000001:00000281
	virt_caps              : hvm hvm_directio
	total_memory           : 16283
	free_memory            : 910
	sharing_freed_memory   : 0
	sharing_used_memory    : 0
	free_cpus              : 0
	xen_major              : 4
	xen_minor              : 2
	xen_extra              : .1_12-1.12.10
	xen_caps               : xen-3.0-x86_64 xen-3.0-x86_32p hvm-3.0-x86_32
	hvm-3.0-x86_32p hvm-3.0-x86_64 
	xen_scheduler          : credit
	xen_pagesize           : 4096
	platform_params        : virt_start=0xffff800000000000
	xen_changeset          : 25952
	xen_commandline        : 
	cc_compiler            : gcc (SUSE Linux) 4.7.2 20130108 [gcc-4_7-branch
	revision 195012
	cc_compile_by          : abuild
	cc_compile_domain      : 
	cc_compile_date        : Wed May 29 20:31:49 UTC 2013
	xend_config_format     : 4

kernel: zeroling

creep invocation:
sudo ./creep -k /root/images/zeroling.img -m <memory> [--no-vif] -c <count>

count - the number of domains created;
memory - the memory per domain in MB;
vif - whether the network interface is present or not;
rate - the domain creation rate in domains per second.

count | memory | vif | rate
------|--------|-----|-----
5 | 16 | no | 27.6
5 | 16 | no | 23.1
5 | 16 | no | 23.7
50 | 16 | no | 18.3
50 | 16 | no | 20.6
50 | 16 | no | 19.3
100 | 16 | no | 15.8
100 | 16 | no | 16.1
100 | 16 | no | 15.4
250 | 16 | no | 12.3
250 | 16 | no | 12.3
250 | 16 | no | 12.3
250 | 16 | yes | 4.5
5 | 32 | yes | 9.2
5 | 32 | yes | 9.9
5 | 32 | yes | 10.2
50 | 32 | yes | 8.2
50 | 32 | yes | 8.2
50 | 32 | yes | 8.6
20 | 128 | no | 16.7
20 | 128 | no | 17.8
20 | 128 | no | 17.1
20 | 128 | yes | 9.2
20 | 128 | yes | 9.4
20 | 128 | yes | 8.5

