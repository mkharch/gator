
creep version: commit f4c7c6

'xl info' output:

	host                   : pod.site
	release                : 3.7.10-1.11-xen
	version                : #1 SMP Thu May 16 20:27:27 UTC 2013 (adf31bb)
	machine                : x86_64
	nr_cpus                : 2
	max_cpu_id             : 1
	nr_nodes               : 1
	cores_per_socket       : 2
	threads_per_core       : 1
	cpu_mhz                : 2394
	hw_caps                :
	bfebfbff:28100800:00000000:00002f40:059ae3bf:00000000:00000001:00000000
	virt_caps              : hvm
	total_memory           : 1992
	free_memory            : 938
	sharing_freed_memory   : 0
	sharing_used_memory    : 0
	free_cpus              : 0
	xen_major              : 4
	xen_minor              : 2
	xen_extra              : .1_12-1.12.10
	xen_caps               : xen-3.0-x86_64 xen-3.0-x86_32p hvm-3.0-x86_32
	hvm-3.0-x86_32p hvm-3.0-x86_64 
	xen_scheduler          : credit
	xen_pagesize           : 4096
	platform_params        : virt_start=0xffff800000000000
	xen_changeset          : 25952
	xen_commandline        : vga=mode-0x31a
	cc_compiler            : gcc (SUSE Linux) 4.7.2 20130108 [gcc-4_7-branch
	revision 195012
	cc_compile_by          : abuild
	cc_compile_domain      : 
	cc_compile_date        : Wed May 29 20:31:49 UTC 2013
	xend_config_format     : 4

kernel: zeroling

creep invocation:
sudo ./creep -k /root/images/zeroling.img -m <memory> [--no-vif] -c <count>

count - the number of domains created;
memory - the memory per domain in MB;
vif - whether the network interface is present or not;
rate - the domain creation rate in domains per second.

count | memory | vif | rate
------|--------|-----|-----
5 | 16 | no | 17.6
5 | 16 | no | 21.0
5 | 16 | no | 20.8
20 | 16 | no | 18.3
20 | 16 | no | 19.3
20 | 16 | no | 18.8
50 | 16 | no | 16.3
50 | 16 | no | 17.6
50 | 16 | no | 16.7
5 | 32 | no | 17.4
5 | 32 | no | 17.9
5 | 32 | no | 21.0
15 | 32 | no | 19.6
15 | 32 | no | 20.2
15 | 32 | no | 18.0
25 | 32 | no | 18.1
25 | 32 | no | 18.0
25 | 32 | no | 15.6
5 | 128 | no | 18.5
5 | 128 | no | 5.2
5 | 128 | no | 6.4
5 | 128 | no | 19.0
5 | 128 | no | 18.4
5 | 128 | no | 16.0
5 | 16 | yes | 4.2 
5 | 16 | yes | 4.1
5 | 16 | yes | 4.4
20 | 16 | yes | 4.1
20 | 16 | yes | 4.3
20 | 16 | yes | 3.4
50 | 16 | yes | failed
5 | 32 | yes | 4.1
5 | 32 | yes | 4.2
5 | 32 | yes | 4.5
15 | 32 | yes | 4.8
15 | 32 | yes | 4.1
15 | 32 | yes | 4.1
25 | 32 | yes | 3.9
25 | 32 | yes | 4.0
25 | 32 | yes | 3.6
5 | 128 | yes | 4.0
5 | 128 | yes | 4.3
5 | 128 | yes | 2.7
5 | 128 | yes | 4.2
5 | 128 | yes | 4.9
5 | 128 | yes | 4.8

