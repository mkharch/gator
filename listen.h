//
//
//

#include <libxl.h>

#define HDR_SIZE	(4 +4)

#define GTR_STATE_REQ	1
#define GTR_STATE_PEND	2
#define GTR_STATE_RSP	3
#define GTR_STATE_DONE	4

#define GTR_MAGIC1		0x27182818
#define GTR_MAGIC2		0x81828172

#define GTR_TAG_ACTION	5
#define GTR_TAG_KERNEL	10
#define GTR_TAG_NAME	11
#define GTR_TAG_MEMORY	12
#define GTR_TAG_EXTRA	13
#define GTR_TAG_BRIDGE	14
#define GTR_TAG_NOVIF	15
#define GTR_TAG_DISK	16
#define GTR_TAG_RDONLY	17
#define GTR_TAG_CONSOLE	18
#define GTR_TAG_PREFIX	20

#define GTR_TAG_OK		100
#define	GTR_TAG_ERROR	110

#define GTR_TAG_DOMAINS	120
#define GTR_TAG_BRIDGES	121

#define GTR_ACTION_CREATE		1
#define GTR_ACTION_CLEANUP		2
#define GTR_ACTION_LIST			3

#define GTR_ACTION_GET_MEMORY	10
#define GTR_ACTION_GET_BRIDGES	11

#define GTR_DOMAIN_RUNNING	1
#define GTR_DOMAIN_BLOCKED	2
#define GTR_DOMAIN_PAUSED	4
#define GTR_DOMAIN_SHUTDOWN	8
#define GTR_DOMAIN_DYING	16

struct request_data {
	int action;
	char *kernel;
	char *name;
	int memory;
	char *extra;
	char *bridge;
	int novif;
	char *disk;
	int rdonly;
	int console;
	char *prefix;
};

struct conn_ctx_t {
	int fd;
	int err;

	int state;
	uint8_t header[HDR_SIZE];
	uint8_t *data;
	int data_size;
	int num_read;
	int num_written;

	struct conn_ctx_t **ref;
	struct conn_ctx_t *next;

	//////////////////////////

	libxl_domid domid;
	libxl_evgen_domain_death *death;	// evenable/evdisable
	libxl_domain_config config;

	void (*action_complete)(libxl_ctx *libxl, struct conn_ctx_t *conn);

	uint32_t fork_console_for_domid;
};

int start_action(libxl_ctx *libxl,
		struct request_data *req_data, struct conn_ctx_t *conn);

void connection_notify_error(struct conn_ctx_t *conn, int rs);
void connection_notify_ok(struct conn_ctx_t *conn);

//EOF
