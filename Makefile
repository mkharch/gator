
.PHONY: default

default: gator

#CFLAGS := -I ../xen/tools/libxc
#CFLAGS += -I ../xen/tools/libxl
CFLAGS += -ggdb

#LDFLAGS += -L ../xen/tools/libxl
#LDFLAGS += -L ../xen/tools/libxc
#LDFLAGS += -Wl,-Bstatic
LDFLAGS += -lxenlight -lxenctrl
LDFLAGS += -Wl,-Bdynamic

OBJS := $(patsubst %.c,%.o,$(wildcard *.c))

%.o: %.c
	gcc -c $(CFLAGS) $<

gator: $(OBJS)
	gcc -o $@ $^ $(LDFLAGS)

