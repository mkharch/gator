#pragma once

#include <stdint.h>

#define MAKE_UINT_32(a, b, c, d) \
	(((uint32_t)(a) << 24) | \
	 ((uint32_t)(b) << 16) | \
	 ((uint32_t)(c) << 8) | \
	  (uint32_t)(d))

#define GET_UINT_16(p) \
	(((uint16_t)((uint8_t *)p)[0] << 8) | \
	  (uint16_t)((uint8_t *)p)[1])

#define GET_UINT_16_LE(p) \
	(((uint16_t)((uint8_t *)p)[1] << 8) | \
	  (uint16_t)((uint8_t *)p)[0])

#define GET_UINT_32(p) \
	(((uint32_t)((uint8_t *)p)[0] << 24) | \
	 ((uint32_t)((uint8_t *)p)[1] << 16) | \
	 ((uint32_t)((uint8_t *)p)[2] << 8) | \
	  (uint32_t)((uint8_t *)p)[3])

#define GET_UINT_32_LE(p) \
	(((uint32_t)((uint8_t *)p)[3] << 24) | \
	 ((uint32_t)((uint8_t *)p)[2] << 16) | \
	 ((uint32_t)((uint8_t *)p)[1] << 8) | \
	  (uint32_t)((uint8_t *)p)[0])

#define GET_UINT_64(p) \
	(((uint64_t)((uint8_t *)p)[0] << 56) | \
	 ((uint64_t)((uint8_t *)p)[1] << 48) | \
	 ((uint64_t)((uint8_t *)p)[2] << 40) | \
	 ((uint64_t)((uint8_t *)p)[3] << 32) | \
	 ((uint64_t)((uint8_t *)p)[4] << 24) | \
	 ((uint64_t)((uint8_t *)p)[5] << 16) | \
	 ((uint64_t)((uint8_t *)p)[6] <<  8) | \
	  (uint64_t)((uint8_t *)p)[7])

#define GET_UINT_64_LE(p) \
	(((uint64_t)((uint8_t *)p)[7] << 56) | \
	 ((uint64_t)((uint8_t *)p)[6] << 48) | \
	 ((uint64_t)((uint8_t *)p)[5] << 40) | \
	 ((uint64_t)((uint8_t *)p)[4] << 32) | \
	 ((uint64_t)((uint8_t *)p)[3] << 24) | \
	 ((uint64_t)((uint8_t *)p)[2] << 16) | \
	 ((uint64_t)((uint8_t *)p)[1] <<  8) | \
	  (uint64_t)((uint8_t *)p)[0])

#define PUT_UINT_16(p, v) do { \
	((uint8_t *)p)[0] = ((uint16_t)(v) >> 8); \
	((uint8_t *)p)[1] =  (uint16_t)(v) & 255; \
} while (0)

#define PUT_UINT_16_LE(p, v) do { \
	((uint8_t *)p)[1] = ((uint16_t)(v) >> 8); \
	((uint8_t *)p)[0] =  (uint16_t)(v) & 255; \
} while (0)

#define PUT_UINT_32(p, v) do { \
	((uint8_t *)p)[0] = ((uint32_t)(v) >> 24); \
	((uint8_t *)p)[1] = ((uint32_t)(v) >> 16) & 255; \
	((uint8_t *)p)[2] = ((uint32_t)(v) >> 8) & 255; \
	((uint8_t *)p)[3] =  (uint32_t)(v) & 255; \
} while (0)

#define PUT_UINT_32_LE(p, v) do { \
	((uint8_t *)p)[3] = ((uint32_t)(v) >> 24); \
	((uint8_t *)p)[2] = ((uint32_t)(v) >> 16) & 255; \
	((uint8_t *)p)[1] = ((uint32_t)(v) >> 8) & 255; \
	((uint8_t *)p)[0] =  (uint32_t)(v) & 255; \
} while (0)

#define PUT_UINT_64(p, v) do { \
	((uint8_t *)p)[0] = ((uint64_t)(v) >> 56); \
	((uint8_t *)p)[1] = ((uint64_t)(v) >> 48) & 255; \
	((uint8_t *)p)[2] = ((uint64_t)(v) >> 40) & 255; \
	((uint8_t *)p)[3] = ((uint64_t)(v) >> 32) & 255; \
	((uint8_t *)p)[4] = ((uint64_t)(v) >> 24) & 255; \
	((uint8_t *)p)[5] = ((uint64_t)(v) >> 16) & 255; \
	((uint8_t *)p)[6] = ((uint64_t)(v) >> 8) & 255; \
	((uint8_t *)p)[7] =  (uint64_t)(v) & 255; \
} while (0)

#define PUT_UINT_64_LE(p, v) do { \
	((uint8_t *)p)[7] = ((uint64_t)(v) >> 56); \
	((uint8_t *)p)[6] = ((uint64_t)(v) >> 48) & 255; \
	((uint8_t *)p)[5] = ((uint64_t)(v) >> 40) & 255; \
	((uint8_t *)p)[4] = ((uint64_t)(v) >> 32) & 255; \
	((uint8_t *)p)[3] = ((uint64_t)(v) >> 24) & 255; \
	((uint8_t *)p)[2] = ((uint64_t)(v) >> 16) & 255; \
	((uint8_t *)p)[1] = ((uint64_t)(v) >> 8) & 255; \
	((uint8_t *)p)[0] =  (uint64_t)(v) & 255; \
} while (0)

//EOF
