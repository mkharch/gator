# A network interface to libXl

The ultimate goal of the project is to create network interface to libxl that is
smaller and faster than libvirt.

# gator

The gator is a command-line utility, which does roughly the same as xl. The
biggest difference is that creep can create multiple domains asynchronously.

Creep options overview:

--verbose

	Increases verbosity of output; can be provided multiple times;

--count <count>

	Create/destroy this many domains; defaults to 1;

--name <name>

	A name of the domain to create; if more than one domain is being created a
numeric suffix is added;

--memory <mb>

	A number of megabytes to allocate per domain;

--kernel </path/to/kernel>

	A path to the file that contains the kernel to boot;

--extra <extra>

	A command line to pass to the kernel;

--bridge <bridge>

	A name of the bridge to attach the virtual network interface to; defaults to "br0";

--no-vif

	Indicates that domain(s) should not have network interfaces; by default a
domain has a network interface;

--console

	Opens a console to the newly created domain; --count must be set to 1;

--cleanup

	Change the requested action from creation (default) to destruction.
