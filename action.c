//
//
//

#include "listen.h"

#include <malloc.h>
#include <assert.h>

#include <libxl_utils.h>

#include "getput.h"

static int action_create(libxl_ctx *libxl,
		struct request_data *req_data, struct conn_ctx_t *conn);
static void action_create_complete(libxl_ctx *libxl, struct conn_ctx_t *conn);

static int action_cleanup(libxl_ctx *libxl,
		struct request_data *req_data, struct conn_ctx_t *conn);
static void action_cleanup_complete(libxl_ctx *libxl, struct conn_ctx_t *conn);

static int action_list(libxl_ctx *libxl,
		struct request_data *req_data, struct conn_ctx_t *conn);

static int action_get_memory(libxl_ctx *libxl, struct request_data *req_data, struct conn_ctx_t *conn);
static int action_get_bridges(libxl_ctx *libxl, struct request_data *req_data, struct conn_ctx_t *conn);

int start_action(libxl_ctx *libxl,
		struct request_data *req_data, struct conn_ctx_t *conn)
{
//	printf("req_data->action = %d\n", req_data->action);
//	printf("req_data->kernel = %s\n", req_data->kernel);
//	printf("req_data->name = %s\n", req_data->name);
//	printf("req_data->memory = %d\n", req_data->memory);
//	printf("req_data->extra = %s\n", req_data->extra);
//	printf("req_data->bridge = %s\n", req_data->bridge);
//	printf("req_data->novif = %d\n", req_data->novif);

	if (req_data->action == GTR_ACTION_CREATE)
		return action_create(libxl, req_data, conn);
	else if (req_data->action == GTR_ACTION_CLEANUP)
		return action_cleanup(libxl, req_data, conn);
	else if (req_data->action == GTR_ACTION_LIST)
		return action_list(libxl, req_data, conn);
	else if (req_data->action == GTR_ACTION_GET_MEMORY)
		return action_get_memory(libxl, req_data, conn);
	else if (req_data->action == GTR_ACTION_GET_BRIDGES)
		return action_get_bridges(libxl, req_data, conn);
	else
		return -1;
}

static int action_create(libxl_ctx *libxl,
		struct request_data *req_data, struct conn_ctx_t *conn)
{
	if (req_data->kernel == 0)
		return -1;
	if (req_data->name == 0)
		return -1;

	libxl_domain_config *dc = &conn->config;
	libxl_domain_config_init(dc);

	libxl_domain_create_info *c_info = &dc->c_info;
	libxl_domain_build_info *b_info = &dc->b_info;

	c_info->type = LIBXL_DOMAIN_TYPE_PV;
	c_info->name = strdup(req_data->name);

	libxl_domain_build_info_init_type(b_info, c_info->type);

	b_info->max_memkb = req_data->memory *1024;
	b_info->target_memkb = req_data->memory *1024;

	if (req_data->extra != 0)
		b_info->u.pv.cmdline = strdup(req_data->extra);
	b_info->u.pv.kernel = strdup(req_data->kernel);

	if (!req_data->novif)
	{
		libxl_device_nic *nic = malloc(sizeof(libxl_device_nic));
		libxl_device_nic_init(nic);

		//nic->script = strdup("/etc/xen/scripts/vif-bridge");
		nic->bridge = strdup(req_data->bridge);

		dc->num_nics = 1;
		dc->nics = nic;
	}

	if (req_data->disk != 0)
	{
		libxl_device_disk *dsk = malloc(sizeof(libxl_device_disk));
		libxl_device_disk_init(dsk);

		dsk->readwrite = !req_data->rdonly;
		dsk->format = LIBXL_DISK_FORMAT_RAW;
		dsk->pdev_path = strdup(req_data->disk);
		dsk->vdev = strdup("xvda"); //any?

		dc->num_disks = 1;
		dc->disks = dsk;
	}

	libxl_asyncop_how ao_how = {
		.u.for_event = (libxl_ev_user) conn,
   	};

	libxl_asyncprogress_how *ao_console_how = 0;
	//libxl_asyncprogress_how ao_console_how_buf = { .callback = 0 };
	//if (conn->console)
	//	ao_console_how = &ao_console_how_buf;
	
	printf("Creating %s...\n", req_data->name);

	int rs = libxl_domain_create_new(libxl,
					dc, &conn->domid, &ao_how, ao_console_how);
	if (rs != 0)
		return -1;

	conn->action_complete = action_create_complete;

	if (req_data->console)
		conn->fork_console_for_domid = (uint32_t)-1;

	return 0;
}

static void action_create_complete(libxl_ctx *libxl, struct conn_ctx_t *conn)
{
	if (conn->fork_console_for_domid)
		conn->fork_console_for_domid = conn->domid;

	int ret = libxl_evenable_domain_death(libxl, conn->domid, 0, &conn->death);
	assert(ret == 0);

	int rs = libxl_domain_unpause(libxl, conn->domid);
	assert(rs == 0); //XXX

	libxl_domain_config_dispose(&conn->config);
}

static int action_cleanup(libxl_ctx *libxl,
		struct request_data *req_data, struct conn_ctx_t *conn)
{
	if (req_data->name == 0)
		return -1;

	libxl_domid domid;
	int rs = libxl_name_to_domid(libxl, req_data->name, &domid);
	if (rs != 0)
		return -1;

	libxl_asyncop_how ao_how = {
		.u.for_event = (libxl_ev_user) conn,
   	};

	printf("Destroying %s...\n", req_data->name);

	rs = libxl_domain_destroy(libxl, domid, &ao_how);
	if (rs != 0)
		return -1;

	conn->action_complete = action_cleanup_complete;

	return 0;
}

static void action_cleanup_complete(libxl_ctx *libxl, struct conn_ctx_t *conn)
{
}

static int action_list(libxl_ctx *libxl,
		struct request_data *req_data, struct conn_ctx_t *conn)
{
	int num_doms;
	libxl_dominfo *dom_list = libxl_list_domain(libxl, &num_doms);
	assert(dom_list != 0);

	uint8_t buffer[16384];
	int buf_off = 0;
	int num_packed = 0;

	int i;
	for (i = 0; i < num_doms; i++)
	{
		libxl_domid domid = dom_list[i].domid;
		char *name = libxl_domid_to_name(libxl, domid);
		if (req_data->prefix == 0 ||
				(name != 0) &&	// dying domain has name (null)
			   		strncmp(name, req_data->prefix, strlen(req_data->prefix)) == 0)
		{
			uint16_t state = 
				((dom_list[i].running) ?GTR_DOMAIN_RUNNING :0) |
				((dom_list[i].blocked) ?GTR_DOMAIN_BLOCKED :0) |
				((dom_list[i].paused) ?GTR_DOMAIN_PAUSED :0) |
				((dom_list[i].shutdown) ?GTR_DOMAIN_SHUTDOWN :0) |
				((dom_list[i].dying) ?GTR_DOMAIN_DYING :0);

			int len = strlen(name);
			assert(sizeof(buffer) -buf_off >= 2 +len +4);
			PUT_UINT_16(buffer +buf_off, len);
			buf_off += 2;
			memcpy(buffer +buf_off, name, len);
			buf_off += len;
			PUT_UINT_32(buffer +buf_off, state);
			buf_off += 4;

			num_packed++;
		}
	}

	libxl_dominfo_list_free(dom_list, num_doms);

	conn->data_size = 4 +4 +2 +4 +buf_off;
	assert(conn->data == 0);
	conn->data = malloc(conn->data_size);
	PUT_UINT_32(conn->data, GTR_MAGIC2);
	PUT_UINT_32(conn->data +4, conn->data_size);
	PUT_UINT_16(conn->data +8, GTR_TAG_DOMAINS);
	PUT_UINT_32(conn->data +10, num_packed);
	memcpy(conn->data +14, buffer, buf_off);

	conn->num_written = 0;
	conn->state = GTR_STATE_RSP;

	return 0;
}

static int action_get_memory(libxl_ctx *libxl, struct request_data *req_data, struct conn_ctx_t *conn)
{
	conn->data_size = 4 +4 +2 +4;
	assert(conn->data == 0);
	conn->data = malloc(conn->data_size);
	PUT_UINT_32(conn->data, GTR_MAGIC2);
	PUT_UINT_32(conn->data +4, conn->data_size);
	PUT_UINT_16(conn->data +8, GTR_TAG_MEMORY);
	PUT_UINT_32(conn->data +10, 137);

	conn->num_written = 0;
	conn->state = GTR_STATE_RSP;
	return 0;
}

static int action_get_bridges(libxl_ctx *libxl, struct request_data *req_data, struct conn_ctx_t *conn)
{
	conn->data_size = 4 +4 +2 +4;
	assert(conn->data == 0);
	conn->data = malloc(conn->data_size);
	PUT_UINT_32(conn->data, GTR_MAGIC2);
	PUT_UINT_32(conn->data +4, conn->data_size);
	PUT_UINT_16(conn->data +8, GTR_TAG_BRIDGES);
	PUT_UINT_32(conn->data +10, 0);

	conn->num_written = 0;
	conn->state = GTR_STATE_RSP;
	return 0;
}

//EOF
